from django.contrib import admin

# Register your models here.
from todos.models import TodoList, TodoItem

# admin.site.register(TodoList)


@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )


@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = (
        "task",
        "due_date",
        "is_completed",
        "list",
    )
